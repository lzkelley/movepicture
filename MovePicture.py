# ==================================================================================================
# MovePicture.py
# --------------
#
# Use a local command to make a movie out of a sequence of images.
#
# Creates a temporary directory which it stories copies of the target files with a simplified
# numbering scheme.
#
# Output Formats:
#  - mpg, mp4  [ mp4 seems to allow more options for FPS ]
#
# ------------------
# Luke Zoltan Kelley
# LKelley@cfa.harvard.edu
# ==================================================================================================

import subprocess
from glob import glob
import os
import shutil
import argparse



class Settings(object):
    '''
    Object for storing settings/parameters. 

    Contains method 'loadArgs' to parse command line arguments, and store to self.
    '''

    prog                = "/opt/local/bin/ffmpeg"                                                   # Program to use for making movie
    fps                 = 32                                                                        # Frames per Second (FPS)
    input_pattern       = "*00.png"                                                                 # Pattern with which to match input files
    output_name         = "movie.mp4"                                                               # Output movie filename and format
    start_frame         = None                                                                      # Frame at which to start movie
    stop_frame          = None                                                                      # Frame at which to stop  movie

    codec               = "libx264"                                                                 # Codec for use with 'prog' for making movie
    tempdir_name        = ".temp"                                                                   # Name of temporary directory for intermediate files
    tempfile_format     = "%04d.png"                                                                # Format for intermediate files in temp directory
    other_args          = ["-pix_fmt","yuv420p"]                                                    # Any additional arguments (no spaces!)
    
    files               = []                                                                        # Store the input files for movie-making


    def loadArgs(self):
        ''' Load and store command line arguments. '''

        parser = argparse.ArgumentParser(description="Convert a set of images into a movie.")

        # Inputs
        parser.add_argument('-i',           metavar='INPUT_PATTERN', type=str, default=self.input_pattern, help='pattern used to find input files (escape wildcard!)'  )
        parser.add_argument('-o',           metavar='OUTPUT_NAME',   type=str, default=self.output_name,   help='name (and filetype) of movie output'                  )
        parser.add_argument('-n', '--fps',  metavar='FPS',           type=int, default=self.fps,           help='frames per second (fps)'                              )
        parser.add_argument('--start',      metavar='START_FRAME',   type=int, default=self.start_frame,   help='frame at which to start movie'                        )
        parser.add_argument('--stop',       metavar='STOP_FRAME',    type=int, default=self.stop_frame,    help='frame at which to stop movie'                         )
        args = parser.parse_args()
        
        # Get settings
        if( args.i       ): self.input_pattern    = args.i
        if( args.o       ): self.output_name      = args.o
        if( args.fps     ): self.fps              = args.fps
        if( args.start   ): self.start_frame      = args.start
        if( args.stop    ): self.stop_frame       = args.stop



def loadFilesDirs(sets):
    '''
    Use the input pattern to find target files.  also create temporary directory.

    Sets 'start_frame' and 'stop_frame' if uninitialized.
    Errors if no files are found, or if temporary directory already exists.
    '''
    allfiles = sorted(glob(sets.input_pattern))

    # Confirm that files are found
    if( allfiles ):
        print " - Found %d files between [ '%s' , '%s' ]" % \
            (len(allfiles), allfiles[0].split("/")[-1], allfiles[-1].split("/")[-1])
    else:
        raise RuntimeError("No files matching '%s' found!" % sets.input_pattern)


    # Set start and stop frames
    if( sets.start_frame == None ): sets.start_frame = 0
    if( sets.stop_frame  == None ): sets.stop_frame  = len(allfiles)-1                              # Number stop inclusively


    # Make sure temporary directory doesn't already exist
    if os.path.isdir(sets.tempdir_name): 
        raise RuntimeError("Temporary directory '%s' already exists!" % sets.tempdir_name)

    # Create directory
    os.makedirs(sets.tempdir_name)

    print " - Using temporary directory '%s'" % (sets.tempdir_name)

    sets.files = allfiles

    return



def copyFiles(sets):
    ''' Copy input files to temporary directory with appropriate file names '''
    print " - Copying files from [%d,%d]." % (sets.start_frame, sets.stop_frame)
    for it, fil in enumerate(sets.files[sets.start_frame:sets.stop_frame]):
        shutil.copyfile(fil, "%s/%s" % (sets.tempdir_name, (sets.tempfile_format % (it)) ))         # Use the fileformat, then insert

    return




def createMovie(sets):
    ''' Create command with input files and arguments to make movie.  run that command. '''
    
    innames = sets.tempdir_name + "/" + sets.tempfile_format
    print " - Creating movie with intermediate files '%s'" % (innames)

    # Create movie-making command
    command = [ sets.prog , \
                #"-i", "%s" % innames, \
                "-r", "%d" % sets.fps, \
                "-i", "%s" % innames, \
                #"-vf", "fps=%d" % sets.fps, \
                "%s" % (sets.output_name) ]

    command[-1:-1] = sets.other_args                                                                # Insert any additional arguments

    # Make a version for printing
    print_command = [ '%s ' % substr for substr in command ]
    print_coomand = ''.join(print_command)
    print " - Making movie with command '%s'" % print_command

    # Call command to make movie
    p = subprocess.call(command)
    return


def cleanDirs(sets):
    ''' Remove temporary directory and its contents '''
    print " - cleaning '%s'" % (sets.tempdir_name)
    shutil.rmtree(sets.tempdir_name)
    return




#  ==================
#  ====  Main()  ====
#  ==================


def main():

    print "\n\nMovieMaker.py\n"
    sets = Settings()                                                                               # Create settings object to store settings
    sets.loadArgs()                                                                                 # Parse command line args and store to settings
    loadFilesDirs(sets)                                                                             # Load files based on input pattern, setup temp directory
    copyFiles(sets)                                                                                 # Copy input files to temporary directory with numbering scheme
    createMovie(sets)                                                                               # Create command and use to make movie
    cleanDirs(sets)                                                                                 # Remove temporary directory and intermediate files
    print "\nDone\n\n"

    return



if __name__ == "__main__": main()

